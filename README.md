# Project Configuration Presets

## Packages

<!-- AUTO-GENERATED-CONTENT:START (SUBPACKAGELIST:verbose=true) -->
* [@koober/commitlint-config](packages/commitlint-config) - Commitlint configuration presets
* [@koober/eslint-config](packages/eslint-config) - ESLint configuration presets
* [@koober/mrm-preset](packages/mrm-preset) - Mrm configuration presets
* [@koober/renovate-config](packages/renovate-config) - Renovate configuration presets
* [@koober/ts-config](packages/ts-config) - Typescript compiler configuration presets
<!-- AUTO-GENERATED-CONTENT:END -->
