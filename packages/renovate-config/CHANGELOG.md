# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.17.1](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.17.0...@koober/renovate-config@2.17.1) (2021-08-31)

**Note:** Version bump only for package @koober/renovate-config





# [2.17.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.16.4...@koober/renovate-config@2.17.0) (2021-08-29)


### Features

* **renovate:** add groupAlgoliaClientJavascript ([c2684b5](https://gitlab.com/koober-sas/project-config/commit/c2684b59c1e8461a0ed4c10c345d49663d8b301e))
* **renovate:** add tsconfig-paths to automerge packages ([d50edb6](https://gitlab.com/koober-sas/project-config/commit/d50edb63fa9f81951c41dea6a1f47f07d177d074))





## [2.16.4](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.16.3...@koober/renovate-config@2.16.4) (2021-08-12)

**Note:** Version bump only for package @koober/renovate-config





## [2.16.3](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.16.2...@koober/renovate-config@2.16.3) (2021-08-11)

**Note:** Version bump only for package @koober/renovate-config





## [2.16.2](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.16.1...@koober/renovate-config@2.16.2) (2021-07-27)

**Note:** Version bump only for package @koober/renovate-config





## [2.16.1](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.16.0...@koober/renovate-config@2.16.1) (2021-07-26)

**Note:** Version bump only for package @koober/renovate-config





# [2.16.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.15.2...@koober/renovate-config@2.16.0) (2021-07-22)


### Features

* **renovate-config:** remove hour limit ([2b51714](https://gitlab.com/koober-sas/project-config/commit/2b517144b1fd7f93edf348ffabc356c675d8e71b))





## [2.15.2](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.15.1...@koober/renovate-config@2.15.2) (2021-07-22)


### Bug Fixes

* **eslint-config:** correct dot-notation compatibility ([c0f23be](https://gitlab.com/koober-sas/project-config/commit/c0f23be7eb69f4d86c7c547ccebd5037ba6a848b))





## [2.15.1](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.15.0...@koober/renovate-config@2.15.1) (2021-07-19)

**Note:** Version bump only for package @koober/renovate-config





# [2.15.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.14.0...@koober/renovate-config@2.15.0) (2021-07-15)


### Features

* add lint-staged to automerge tools ([3f9d091](https://gitlab.com/koober-sas/project-config/commit/3f9d091e255cfb9250d2944d1849a24bbca8d4df))





# [2.14.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.13.2...@koober/renovate-config@2.14.0) (2021-07-13)


### Features

* set automerge on nodemon ([f0a5338](https://gitlab.com/koober-sas/project-config/commit/f0a533894f6584f59a2949eba26ce03c41f8c163))





## [2.13.2](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.13.1...@koober/renovate-config@2.13.2) (2021-07-09)

**Note:** Version bump only for package @koober/renovate-config





## [2.13.1](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.13.0...@koober/renovate-config@2.13.1) (2021-07-07)

**Note:** Version bump only for package @koober/renovate-config





# [2.13.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.12.1...@koober/renovate-config@2.13.0) (2021-07-02)


### Features

* add ts-node, ts-node-dev to automerge tools ([8ef6bda](https://gitlab.com/koober-sas/project-config/commit/8ef6bdaa066e1361dc6b029a81e8cc53301d8fc6))





## [2.12.1](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.12.0...@koober/renovate-config@2.12.1) (2021-06-02)

**Note:** Version bump only for package @koober/renovate-config





# [2.12.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.11.1...@koober/renovate-config@2.12.0) (2021-05-27)


### Features

* automerge minor typescript version as they are validated by hooks and tests ([c3241e3](https://gitlab.com/koober-sas/project-config/commit/c3241e3177439f08d127a190b3ed8a6329e9d4a8))





## [2.11.1](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.11.0...@koober/renovate-config@2.11.1) (2021-05-26)


### Bug Fixes

* **mrm-preset:** correct automerge jest settings ([77da87f](https://gitlab.com/koober-sas/project-config/commit/77da87facff826a78eb08e6195d4daafb3cc724b))





# [2.11.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.10.0...@koober/renovate-config@2.11.0) (2021-05-17)


### Features

* **mrm:** add koober std ([10b55be](https://gitlab.com/koober-sas/project-config/commit/10b55bed30c9f5b08db8d343cfcdd68d7d318fb1))





# [2.10.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.9...@koober/renovate-config@2.10.0) (2021-05-14)


### Features

* **mrm:** automerge @babel/eslint-parser package ([00b20a4](https://gitlab.com/koober-sas/project-config/commit/00b20a49411f220e5ce3df98b3cd4fbb9063bfa0))





## [2.9.9](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.8...@koober/renovate-config@2.9.9) (2021-05-06)

**Note:** Version bump only for package @koober/renovate-config





## [2.9.8](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.7...@koober/renovate-config@2.9.8) (2021-05-06)

**Note:** Version bump only for package @koober/renovate-config





## [2.9.7](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.6...@koober/renovate-config@2.9.7) (2021-05-05)

**Note:** Version bump only for package @koober/renovate-config





## [2.9.6](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.5...@koober/renovate-config@2.9.6) (2021-04-26)

**Note:** Version bump only for package @koober/renovate-config





## [2.9.5](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.4...@koober/renovate-config@2.9.5) (2021-04-23)

**Note:** Version bump only for package @koober/renovate-config





## [2.9.4](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.3...@koober/renovate-config@2.9.4) (2021-04-22)

**Note:** Version bump only for package @koober/renovate-config





## [2.9.3](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.2...@koober/renovate-config@2.9.3) (2021-04-21)

**Note:** Version bump only for package @koober/renovate-config





## [2.9.2](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.1...@koober/renovate-config@2.9.2) (2021-04-20)

**Note:** Version bump only for package @koober/renovate-config





## [2.9.1](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.9.0...@koober/renovate-config@2.9.1) (2021-04-19)


### Bug Fixes

* **renovate:** fix tests ([fb35e13](https://gitlab.com/koober-sas/project-config/commit/fb35e137d1492bc7d677fb5d49541028fcffd854))





# [2.9.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.8.3...@koober/renovate-config@2.9.0) (2021-04-09)


### Features

* **renovate:** add automerge on babel core ([df605bb](https://gitlab.com/koober-sas/project-config/commit/df605bbe6719b908188c6cde3e935b59981821c9))





## [2.8.3](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.8.2...@koober/renovate-config@2.8.3) (2021-03-30)

**Note:** Version bump only for package @koober/renovate-config





## [2.8.2](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.8.1...@koober/renovate-config@2.8.2) (2021-03-22)

**Note:** Version bump only for package @koober/renovate-config





## [2.8.1](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.8.0...@koober/renovate-config@2.8.1) (2021-03-16)

**Note:** Version bump only for package @koober/renovate-config





# [2.8.0](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.14...@koober/renovate-config@2.8.0) (2021-03-12)


### Features

* **renovate:** add semantic-release to automerge tools ([109489d](https://gitlab.com/koober-sas/project-config/commit/109489dd2d57389b67cbbde85a062f95c21f0271))





## [2.7.14](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.13...@koober/renovate-config@2.7.14) (2021-03-10)


### Bug Fixes

* **mrm:** correct patch-package name ([e2108b4](https://gitlab.com/koober-sas/project-config/commit/e2108b43d4342f472de5983a945e07c7d3a96dc7))





## [2.7.13](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.12...@koober/renovate-config@2.7.13) (2021-03-01)


### Bug Fixes

* **mrm:** correct automerge typescript ([4663615](https://gitlab.com/koober-sas/project-config/commit/46636155fb78a14cde50c7b1f1e13700d5df678a))





## [2.7.12](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.11...@koober/renovate-config@2.7.12) (2021-02-25)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.11](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.10...@koober/renovate-config@2.7.11) (2021-02-24)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.10](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.9...@koober/renovate-config@2.7.10) (2021-02-23)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.9](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.8...@koober/renovate-config@2.7.9) (2021-02-22)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.8](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.7...@koober/renovate-config@2.7.8) (2021-02-19)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.7](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.6...@koober/renovate-config@2.7.7) (2021-02-15)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.6](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.5...@koober/renovate-config@2.7.6) (2021-02-12)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.5](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.4...@koober/renovate-config@2.7.5) (2021-02-01)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.4](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.3...@koober/renovate-config@2.7.4) (2021-01-29)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.3](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.2...@koober/renovate-config@2.7.3) (2021-01-27)

**Note:** Version bump only for package @koober/renovate-config





## [2.7.2](https://gitlab.com/koober-sas/project-config/compare/@koober/renovate-config@2.7.1...@koober/renovate-config@2.7.2) (2021-01-18)

**Note:** Version bump only for package @koober/renovate-config
