# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.4.1](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.4.0...@koober/ts-config@1.4.1) (2021-08-29)

**Note:** Version bump only for package @koober/ts-config





# [1.4.0](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.3.0...@koober/ts-config@1.4.0) (2021-07-26)


### Features

* **ts-config:** enable noImplicitOverride check ([ce2c8e2](https://gitlab.com/koober-sas/project-config/commit/ce2c8e26f48b6cb997e74b4c12c9824329fd14be))





# [1.3.0](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.26...@koober/ts-config@1.3.0) (2021-07-22)


### Bug Fixes

* **eslint-config:** correct dot-notation compatibility ([c0f23be](https://gitlab.com/koober-sas/project-config/commit/c0f23be7eb69f4d86c7c547ccebd5037ba6a848b))


### Features

* **tsconfig:** enable noPropertyAccessFromIndexSignature ([12b6a35](https://gitlab.com/koober-sas/project-config/commit/12b6a35498120a7c47d3f9e5f5e90c41b3ff19fe))
* **tsconfig:** enable noUncheckedIndexedAccess ([f23f853](https://gitlab.com/koober-sas/project-config/commit/f23f85371e146d4b3772f78818cc43246b897dff))





## [1.2.26](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.25...@koober/ts-config@1.2.26) (2021-07-02)

**Note:** Version bump only for package @koober/ts-config





## [1.2.25](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.24...@koober/ts-config@1.2.25) (2021-05-27)

**Note:** Version bump only for package @koober/ts-config





## [1.2.24](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.23...@koober/ts-config@1.2.24) (2021-04-22)

**Note:** Version bump only for package @koober/ts-config





## [1.2.23](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.22...@koober/ts-config@1.2.23) (2021-04-21)

**Note:** Version bump only for package @koober/ts-config





## [1.2.22](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.21...@koober/ts-config@1.2.22) (2021-04-09)

**Note:** Version bump only for package @koober/ts-config





## [1.2.21](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.20...@koober/ts-config@1.2.21) (2021-03-10)

**Note:** Version bump only for package @koober/ts-config





## [1.2.20](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.19...@koober/ts-config@1.2.20) (2021-02-24)

**Note:** Version bump only for package @koober/ts-config





## [1.2.19](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.18...@koober/ts-config@1.2.19) (2021-02-12)

**Note:** Version bump only for package @koober/ts-config





## [1.2.18](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.17...@koober/ts-config@1.2.18) (2021-01-19)

**Note:** Version bump only for package @koober/ts-config





## [1.2.17](https://gitlab.com/koober-sas/project-config/compare/@koober/ts-config@1.2.16...@koober/ts-config@1.2.17) (2021-01-18)

**Note:** Version bump only for package @koober/ts-config
