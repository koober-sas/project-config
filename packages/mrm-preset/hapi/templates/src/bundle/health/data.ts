// Model / Data module

// eslint-disable-next-line no-shadow
export enum HealthStatus {
  Pass = 'pass',
  Warn = 'warn',
  Fail = 'fail',
}
