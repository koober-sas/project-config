/* cSpell: disable */
// @ts-check

// @ts-ignore
const { intersect } = require('semver-intersect');
const { packageJson } = require('mrm-core');
const jsonFile = require('./jsonFile');

/**
 * An empty placeholder for npm script
 */
const emptyScript = ':';

/**
 * @param {(pkg: import('mrm-core').PackageJson) => void} block
 */
function withPackageJson(block) {
  const packageFile = packageJson();
  block(packageFile);
  packageFile.save();
}

/**
 * @param {import('mrm-core').PackageJson} packageFile
 * @param {{
 *   name: string,
 *   state: 'present'|'absent'|'default',
 *   script: string,
 * }} options
 */
function script(packageFile, { name, state, script: scriptName }) {
  if (state === 'absent') {
    packageFile.removeScript(name);
  } else if (state === 'present' || (state === 'default' && !packageFile.getScript(name))) {
    packageFile.setScript(name, scriptName);
  }
}

/**
 *
 * @param {import('mrm-core').PackageJson} packageFile
 * @param {Record<string, string>} engineVersionMap
 */
function engineMinVersion(packageFile, engineVersionMap) {
  /**
   * @param {string} engineName
   */
  const engineConstraint = (engineName) => {
    const defaultVersion = engineVersionMap[engineName];
    const currentVersion = packageFile.get(`engines.${engineName}`, defaultVersion);
    try {
      return intersect(currentVersion, defaultVersion);
    } catch (_) {
      return currentVersion; // leave unchanged
    }
  };

  packageFile.merge({
    engines: Object.keys(engineVersionMap).reduce((acc, engineName) => {
      return {
        ...acc,
        [engineName]: engineConstraint(engineName),
      };
    }, {}),
  });
}

module.exports = {
  ...jsonFile,
  emptyScript,
  script,
  engineMinVersion,
  withPackageJson,
};
