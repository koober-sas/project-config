# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.30.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.29.0...@koober/mrm-preset@1.30.0) (2021-08-31)


### Features

* add code analysis placeholder ([eab6351](https://gitlab.com/koober-sas/project-config/commit/eab6351b1bb3d07e0fd18570753fce3c7aac1897))
* **mrm-preset:** add code analysis ci task ([8cf5556](https://gitlab.com/koober-sas/project-config/commit/8cf5556b8b3255d33248b850bc65578b212f2b40))
* **mrm-preset:** add code analysis gitlab mixin ([80647ed](https://gitlab.com/koober-sas/project-config/commit/80647edaa35f9aac886f73ab52bf396c3dfc19c0))





# [1.29.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.28.1...@koober/mrm-preset@1.29.0) (2021-08-11)


### Bug Fixes

* **mrm:** fix command develop to load dotenv ([9116972](https://gitlab.com/koober-sas/project-config/commit/9116972654925c47c0e4a37227b647a39503073d))


### Features

* **eslint-config:** add recommended settings for typescript ([cb13c21](https://gitlab.com/koober-sas/project-config/commit/cb13c2130f24039079776c9e80477806ab5c8bcd))
* **mrm-preset:** add default cspell task ([f654dfc](https://gitlab.com/koober-sas/project-config/commit/f654dfc11c6f45780b13a113b332bf49759c7edd))





## [1.28.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.28.0...@koober/mrm-preset@1.28.1) (2021-07-27)


### Bug Fixes

* **mrm-config:** run build before on lint and test ([0bfc3ae](https://gitlab.com/koober-sas/project-config/commit/0bfc3ae490a12fc839007cc0b0b414c3a575ccdb))





# [1.28.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.27.0...@koober/mrm-preset@1.28.0) (2021-07-26)


### Bug Fixes

* **mrm-preset:** correct lint errors ([fb0f4ac](https://gitlab.com/koober-sas/project-config/commit/fb0f4ac4a9821e29e688e7a0c8e0514a8bcb51e5))


### Features

* **eslint-config:** enable @typescript-eslint/strict-boolean-expressions rule ([f2abe7c](https://gitlab.com/koober-sas/project-config/commit/f2abe7cdc75fc1fb4201c262867b26acf6ca7305))





# [1.27.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.26.2...@koober/mrm-preset@1.27.0) (2021-07-22)


### Features

* **mrm-preset:** add NODE_ENV=development for ts-node-dev ([8e0eb65](https://gitlab.com/koober-sas/project-config/commit/8e0eb6580bc1d675dadae1c405928af74ef1449c))





## [1.26.2](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.26.1...@koober/mrm-preset@1.26.2) (2021-07-22)


### Bug Fixes

* correct @typescript-eslint/dot-notation ([80dacb7](https://gitlab.com/koober-sas/project-config/commit/80dacb760cb07662d6e6c5c20924a7262bec1541))





## [1.26.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.26.0...@koober/mrm-preset@1.26.1) (2021-07-22)


### Bug Fixes

* correct CODEOWNERS generation ([37e846b](https://gitlab.com/koober-sas/project-config/commit/37e846b965f39911bdceacb83dc161c1e60b5bbb))
* correct file() call ([20a1f27](https://gitlab.com/koober-sas/project-config/commit/20a1f27e3e61a21e4202278de51f519b34d4591f))
* **mrm-preset:** correct deploy step ([f7b06da](https://gitlab.com/koober-sas/project-config/commit/f7b06da2e3a001fe26aa53a11c6dbd716adb87d2))





# [1.26.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.25.0...@koober/mrm-preset@1.26.0) (2021-07-20)


### Bug Fixes

* correct release button on release commit ([75835fb](https://gitlab.com/koober-sas/project-config/commit/75835fb07dc9bdff5f6a6c6ffabfba561c50ef4f))


### Features

* **mrm-preset:** add CODE_OF_CONDUCT generator ([d4bc97d](https://gitlab.com/koober-sas/project-config/commit/d4bc97d547191486f6014015fd195cc05f89a38c))





# [1.25.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.24.0...@koober/mrm-preset@1.25.0) (2021-07-19)


### Bug Fixes

* **mrm-preset:** hide release button on release commit ([d177504](https://gitlab.com/koober-sas/project-config/commit/d177504356e4f86fbb687d40ba9dc2f8c3c88da4))


### Features

* **mrm-preset:** add CODEOWNERS file ([a367ca5](https://gitlab.com/koober-sas/project-config/commit/a367ca5c360c9c53efe527016f9bfa673ecb1b1f))





# [1.24.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.23.0...@koober/mrm-preset@1.24.0) (2021-07-13)


### Bug Fixes

* correct release button presence in mrm ([10a143c](https://gitlab.com/koober-sas/project-config/commit/10a143c1830cae5b27f80d1ee26ea3febc5790a5))


### Features

* add default pages task in gitlab ci ([bf86943](https://gitlab.com/koober-sas/project-config/commit/bf86943e85d13404211f31903bd680119ab0e9a0))





# [1.23.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.22.5...@koober/mrm-preset@1.23.0) (2021-07-13)


### Bug Fixes

* add public/ to gitignore folders ([62962e6](https://gitlab.com/koober-sas/project-config/commit/62962e6f30311ef078a7b28308daf636dafeb9c5))
* enforce npm when publishing to avoid multi standards ([23e25ab](https://gitlab.com/koober-sas/project-config/commit/23e25abd82287a3e8984ecbafaed8dad087c01be))
* improve setup documentation for npm publish ([4489bbe](https://gitlab.com/koober-sas/project-config/commit/4489bbe744b4a8f42347dc78b96b7a0a4aac9153))
* use --yes in lerna publish in CI environment ([59e3e11](https://gitlab.com/koober-sas/project-config/commit/59e3e1117c61fd9a3db39f0dd725a7654c3730c0))


### Features

* add /public to ignored path ([171e0c9](https://gitlab.com/koober-sas/project-config/commit/171e0c97561d7d493af17eedb57da6a75d88c819))





## [1.22.5](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.22.4...@koober/mrm-preset@1.22.5) (2021-07-09)


### Bug Fixes

* correct syncDirectory typing error VS runtime ([9f7570e](https://gitlab.com/koober-sas/project-config/commit/9f7570e888af22e2801b648ff43f0ada3daa9752))





## [1.22.4](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.22.3...@koober/mrm-preset@1.22.4) (2021-05-26)


### Bug Fixes

* add --ci option to avoid jest snapshot update ([fb2b8ea](https://gitlab.com/koober-sas/project-config/commit/fb2b8ead98dd2e909ee5cc7fae07589b5e453f8b))





## [1.22.3](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.22.2...@koober/mrm-preset@1.22.3) (2021-05-14)

**Note:** Version bump only for package @koober/mrm-preset





## [1.22.2](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.22.1...@koober/mrm-preset@1.22.2) (2021-05-05)


### Bug Fixes

* **mrm:** correct include path ([74ba64a](https://gitlab.com/koober-sas/project-config/commit/74ba64aeb770b56caa3d47b818def07804ab72e0))





## [1.22.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.22.0...@koober/mrm-preset@1.22.1) (2021-05-05)


### Bug Fixes

* **mrm:** correct include script for gitlab ci ([fa9b9e4](https://gitlab.com/koober-sas/project-config/commit/fa9b9e4dbb4d0a59bf564614f93c20c7d40f400c))





# [1.22.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.21.1...@koober/mrm-preset@1.22.0) (2021-05-05)


### Bug Fixes

* **mrm:** change log level cli option name ([24f2570](https://gitlab.com/koober-sas/project-config/commit/24f2570dfb2bdca4fc0a809e4b2e6b9a2832a42e))
* **mrm:** remove trust level option ([b15d6bd](https://gitlab.com/koober-sas/project-config/commit/b15d6bdecdd131ff4bd087ead68c402d7d329842))
* **mrm:** remove warning that should use the global command ([857fa57](https://gitlab.com/koober-sas/project-config/commit/857fa57db81effb2dbb697a4bb18817b2ef4d045))


### Features

* use .gitlab folder instead of .autodevops ([46cbd2d](https://gitlab.com/koober-sas/project-config/commit/46cbd2d40825206311b5a3443116ecb8716d7249))





## [1.21.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.21.0...@koober/mrm-preset@1.21.1) (2021-04-23)


### Bug Fixes

* set semantic-release for non workspace projects ([bd15432](https://gitlab.com/koober-sas/project-config/commit/bd1543284068e4c9c3e073bcc93e6e1cacb6f98f))





# [1.21.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.20.1...@koober/mrm-preset@1.21.0) (2021-04-22)


### Bug Fixes

* **mrm:** correct vscode eslint settings ([3ec73fb](https://gitlab.com/koober-sas/project-config/commit/3ec73fbdcd7f5fcc0bc0fabcf178d6d2975a88c8))


### Features

* **mrm:** remove prebuild scripts ([261f7df](https://gitlab.com/koober-sas/project-config/commit/261f7df8e629b9cb10bb3360f56f90c0d196e8f4))
* **mrm:** remove prebuild scripts ([64ff21b](https://gitlab.com/koober-sas/project-config/commit/64ff21b4b1de5b536978d26f902237259e9a69ed))





## [1.20.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.20.0...@koober/mrm-preset@1.20.1) (2021-04-21)


### Bug Fixes

* **mrm:** correct typescript detection ([98ea33c](https://gitlab.com/koober-sas/project-config/commit/98ea33c0a54b4b478e668f5c372497b4b7a677c0))





# [1.20.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.19.1...@koober/mrm-preset@1.20.0) (2021-04-21)


### Bug Fixes

* **mrm:** correct tsc lint-staged command ([e474888](https://gitlab.com/koober-sas/project-config/commit/e4748883fbd3026ab5b34c11215e782a35bbbe9b))


### Features

* add json eslint support in vscode ([4f39f20](https://gitlab.com/koober-sas/project-config/commit/4f39f20d42845e4777dd70394ba9091bac19364e))





## [1.19.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.19.0...@koober/mrm-preset@1.19.1) (2021-04-21)


### Bug Fixes

* **mrm:** avoid using new ?? syntax for better compatibility ([0d09135](https://gitlab.com/koober-sas/project-config/commit/0d09135d5e99a5f7ff10be9d208ffb179bf95e0a))





# [1.19.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.18.1...@koober/mrm-preset@1.19.0) (2021-04-20)


### Features

* **mrm:** add lint-staged ([dc8be06](https://gitlab.com/koober-sas/project-config/commit/dc8be06d9ef2f3443fefb1ebfcaa319ec9e4057c))





## [1.18.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.18.0...@koober/mrm-preset@1.18.1) (2021-04-20)


### Bug Fixes

* disable husky in ci environment ([bdbb1d7](https://gitlab.com/koober-sas/project-config/commit/bdbb1d71b3aee850c657abc297476d88e9110636))





# [1.18.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.17.6...@koober/mrm-preset@1.18.0) (2021-04-20)


### Features

* **mrm:** add cspell ([59879a3](https://gitlab.com/koober-sas/project-config/commit/59879a367a5731fd52e5cafa7cef0d0648468cca))





## [1.17.6](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.17.5...@koober/mrm-preset@1.17.6) (2021-04-19)


### Bug Fixes

* **deps:** update dependency mrm-core to v6 ([96027ca](https://gitlab.com/koober-sas/project-config/commit/96027ca1709f92651ca7aaec8e2cfeb6c76c7d53))





## [1.17.5](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.17.4...@koober/mrm-preset@1.17.5) (2021-04-09)


### Bug Fixes

* **mrm:** avoid abbreviations ([39a1061](https://gitlab.com/koober-sas/project-config/commit/39a1061fe63012153a804a6ecc56e086b0b7d977))





## [1.17.4](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.17.3...@koober/mrm-preset@1.17.4) (2021-03-30)


### Bug Fixes

* **mrm:** correct renovate ([ed05c81](https://gitlab.com/koober-sas/project-config/commit/ed05c81ad3bd94ac64b8ab784642235e213d60f5))





## [1.17.3](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.17.2...@koober/mrm-preset@1.17.3) (2021-03-30)


### Bug Fixes

* **mrm:** correct duplicate hooks ([b893395](https://gitlab.com/koober-sas/project-config/commit/b893395936b158c61bf0179829c848ab094713e3))





## [1.17.2](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.17.1...@koober/mrm-preset@1.17.2) (2021-03-30)


### Bug Fixes

* **mrm:** correct ts-node-dev presence ([0b03cad](https://gitlab.com/koober-sas/project-config/commit/0b03cad827bb50b79db464ff47e3631a590902d9))





## [1.17.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.17.0...@koober/mrm-preset@1.17.1) (2021-03-30)


### Bug Fixes

* **mrm:** correct misc bugs ([8439a40](https://gitlab.com/koober-sas/project-config/commit/8439a4050af78970dd10e21871529a7438ae5357))





# [1.17.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.10...@koober/mrm-preset@1.17.0) (2021-03-30)


### Bug Fixes

* change postinstall to prepare ([ff0fac6](https://gitlab.com/koober-sas/project-config/commit/ff0fac618bc4bad304df1f007e00fee8e5170b5f))
* **mrm:** clean removed typedoc excludeNotExported ([0f61a44](https://gitlab.com/koober-sas/project-config/commit/0f61a4412a1153468f662c0ffd116681cc3685a1))


### Features

* **mrm:** migrate to husky v6 ([fce6188](https://gitlab.com/koober-sas/project-config/commit/fce6188ce599d22f851119c3de8e8561219213e8))





## [1.16.10](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.9...@koober/mrm-preset@1.16.10) (2021-03-23)


### Bug Fixes

* **mrm:** avoid installing ts-node-dev for libraries and workspace ([ac30f9a](https://gitlab.com/koober-sas/project-config/commit/ac30f9a594f077f37733ace79669abb4c50f36c6))





## [1.16.9](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.8...@koober/mrm-preset@1.16.9) (2021-03-22)


### Bug Fixes

* **eslint:** remove @typescript-eslint/parser, @typescript-eslint/eslint-plugin from dev deps ([bbf751f](https://gitlab.com/koober-sas/project-config/commit/bbf751fba9f704f1adca69fc38a816a1f007ea62))





## [1.16.8](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.7...@koober/mrm-preset@1.16.8) (2021-03-10)


### Bug Fixes

* **mrm:** correct semantic release peer deps ([ea24ec9](https://gitlab.com/koober-sas/project-config/commit/ea24ec93a7c8e225cb07edb30b2780b37d5ab157))





## [1.16.7](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.6...@koober/mrm-preset@1.16.7) (2021-03-10)

**Note:** Version bump only for package @koober/mrm-preset





## [1.16.6](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.5...@koober/mrm-preset@1.16.6) (2021-03-01)


### Bug Fixes

* **deps:** update dependency mrm-core to v4.7.0 ([1ec14e6](https://gitlab.com/koober-sas/project-config/commit/1ec14e612e6bda6280d1412ea98873038b326fb0))
* **deps:** update dependency sync-directory to v2.2.17 ([72b2650](https://gitlab.com/koober-sas/project-config/commit/72b2650b689667177d67581cf67de645e36df7e7))





## [1.16.5](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.4...@koober/mrm-preset@1.16.5) (2021-02-25)


### Bug Fixes

* **mrm:** correct lerna publish message ([4c3b1ff](https://gitlab.com/koober-sas/project-config/commit/4c3b1ffab9a97b2a70fac6862ac0d609b09ed1cd))





## [1.16.4](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.3...@koober/mrm-preset@1.16.4) (2021-02-24)


### Bug Fixes

* **mrm:** add lerna conventional commit when version is created ([b3de274](https://gitlab.com/koober-sas/project-config/commit/b3de27497dba7869de1894571d3c11f51e891df0))





## [1.16.3](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.2...@koober/mrm-preset@1.16.3) (2021-02-22)


### Bug Fixes

* ts-node-dev dependencies should be present ([a50b8d3](https://gitlab.com/koober-sas/project-config/commit/a50b8d3eddaec42cbe602b4c37a65de182a43a5a))





## [1.16.2](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.1...@koober/mrm-preset@1.16.2) (2021-02-22)


### Reverts

* @koober/mrm-preset@1.16.2 ([3854a81](https://gitlab.com/koober-sas/project-config/commit/3854a81265c4b5486f938a1403d3a8aa56753449))





## [1.16.2](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.1...@koober/mrm-preset@1.16.2) (2021-02-19)

**Note:** Version bump only for package @koober/mrm-preset





## [1.16.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.16.0...@koober/mrm-preset@1.16.1) (2021-02-15)


### Bug Fixes

* **deps:** update dependency mrm-core to v4.6.0 ([c0f4a38](https://gitlab.com/koober-sas/project-config/commit/c0f4a38bd8a073f8ae2a5395cdcc1ed2da375e7c))
* **mrm:** remove cache to avoid ci errors on outdated yarn.lock ([ee2fa63](https://gitlab.com/koober-sas/project-config/commit/ee2fa6399f019538054f82bcbb6e96c986aaef13))





# [1.16.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.15.3...@koober/mrm-preset@1.16.0) (2021-02-12)


### Bug Fixes

* **deps:** update dependency mrm-core to v4.4.0 ([b9058cf](https://gitlab.com/koober-sas/project-config/commit/b9058cfd12ae47166594f72bd4f28130bbc482c4))
* **deps:** update dependency sync-directory to v2.2.13 ([6ce0a83](https://gitlab.com/koober-sas/project-config/commit/6ce0a833575cb334a56a5f20e1ee31911e777901))
* **deps:** update dependency sync-directory to v2.2.14 ([2f4cf0a](https://gitlab.com/koober-sas/project-config/commit/2f4cf0ac051211329578bfa144957d71be295cd1))
* **deps:** update dependency sync-directory to v2.2.15 ([10c32e9](https://gitlab.com/koober-sas/project-config/commit/10c32e9e76e315ad9ac6ce3f53acd0a2d2cab153))
* **mrm:** rollback to hardlink copy ([441cc86](https://gitlab.com/koober-sas/project-config/commit/441cc86ff70510cf82fa782e1e3a06b5b4e7bea1))


### Features

* **mrm:** add install retry ([f255526](https://gitlab.com/koober-sas/project-config/commit/f255526ef485b8732ac8e143b9afe3a3a4b3afb3))
* **mrm:** migrate from babel-eslint to @babel/eslint-parser ([35a85c0](https://gitlab.com/koober-sas/project-config/commit/35a85c0056a972d4bb2faaaf030a9143acb89693))





## [1.15.3](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.15.2...@koober/mrm-preset@1.15.3) (2021-02-01)


### Bug Fixes

* **mrm:** use copy instead of hardlink ([a3cde2e](https://gitlab.com/koober-sas/project-config/commit/a3cde2e34043b8366ce77d098fdbb7ab73e897e3))





## [1.15.2](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.15.1...@koober/mrm-preset@1.15.2) (2021-02-01)


### Bug Fixes

* **mrm:** correct release and deploy job visibility and behavior ([bbe46fb](https://gitlab.com/koober-sas/project-config/commit/bbe46fbcfe4d7083cf25e8cd246206cd0060248c))





## [1.15.1](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.15.0...@koober/mrm-preset@1.15.1) (2021-01-29)


### Bug Fixes

* **mrm:** filter deploy only on master branch ([c88191d](https://gitlab.com/koober-sas/project-config/commit/c88191db75415c3026794264448e27db5ef687e0))





# [1.15.0](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.14.6...@koober/mrm-preset@1.15.0) (2021-01-29)


### Features

* **mrm:** add generic jobs for deploy ([7495d9e](https://gitlab.com/koober-sas/project-config/commit/7495d9e0c7ad08b3c2a7eaec1144d733e0b1bc01))





## [1.14.6](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.14.5...@koober/mrm-preset@1.14.6) (2021-01-27)


### Bug Fixes

* **mrm:** remove conflictuous package ([569b39b](https://gitlab.com/koober-sas/project-config/commit/569b39bc058c4bb54a412f33514e6e3019ccf81f))





## [1.14.5](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.14.4...@koober/mrm-preset@1.14.5) (2021-01-27)


### Bug Fixes

* **mrm:** add missing peer deps for babel-jest ([b906752](https://gitlab.com/koober-sas/project-config/commit/b90675265ec149d6c67d9c5a1ef3eab917782dff))
* **mrm:** correct ci devops artifacts naming ([700f58f](https://gitlab.com/koober-sas/project-config/commit/700f58fa34cc5df7a9bd3e8becd960c517d9809c))





## [1.14.4](https://gitlab.com/koober-sas/project-config/compare/@koober/mrm-preset@1.14.3...@koober/mrm-preset@1.14.4) (2021-01-18)


### Bug Fixes

* **gitlab:** remove trigger release button ([2df7711](https://gitlab.com/koober-sas/project-config/commit/2df77111c0cdb06f9b10d81f975f246383f1ff94))
