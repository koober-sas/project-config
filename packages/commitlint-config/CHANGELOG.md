# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.11](https://gitlab.com/koober-sas/project-config/compare/@koober/commitlint-config@1.0.10...@koober/commitlint-config@1.0.11) (2021-08-11)

**Note:** Version bump only for package @koober/commitlint-config





## [1.0.10](https://gitlab.com/koober-sas/project-config/compare/@koober/commitlint-config@1.0.9...@koober/commitlint-config@1.0.10) (2021-07-22)


### Bug Fixes

* **eslint-config:** correct dot-notation compatibility ([c0f23be](https://gitlab.com/koober-sas/project-config/commit/c0f23be7eb69f4d86c7c547ccebd5037ba6a848b))





## [1.0.9](https://gitlab.com/koober-sas/project-config/compare/@koober/commitlint-config@1.0.8...@koober/commitlint-config@1.0.9) (2021-04-22)

**Note:** Version bump only for package @koober/commitlint-config





## [1.0.8](https://gitlab.com/koober-sas/project-config/compare/@koober/commitlint-config@1.0.7...@koober/commitlint-config@1.0.8) (2021-02-24)

**Note:** Version bump only for package @koober/commitlint-config





## [1.0.7](https://gitlab.com/koober-sas/project-config/compare/@koober/commitlint-config@1.0.6...@koober/commitlint-config@1.0.7) (2021-01-18)

**Note:** Version bump only for package @koober/commitlint-config
