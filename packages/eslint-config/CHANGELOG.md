# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [5.12.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.11.3...@koober/eslint-config@5.12.0) (2021-08-31)


### Features

* **eslint-config:** add pure functional configuration ([8df5e90](https://gitlab.com/koober-sas/project-config/commit/8df5e908fba9386bb9c74cfdc1b05ad1388e7193))
* remove flowtype ([9ba2422](https://gitlab.com/koober-sas/project-config/commit/9ba2422fb5a290e15333f6310e5df8de8b534461))





## [5.11.3](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.11.2...@koober/eslint-config@5.11.3) (2021-08-31)

**Note:** Version bump only for package @koober/eslint-config





## [5.11.2](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.11.1...@koober/eslint-config@5.11.2) (2021-08-29)

**Note:** Version bump only for package @koober/eslint-config





## [5.11.1](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.11.0...@koober/eslint-config@5.11.1) (2021-08-12)


### Bug Fixes

* disable jest/expect-expect ([af10796](https://gitlab.com/koober-sas/project-config/commit/af107966a9efa7ca77ff12103f2c09a76dbc1556))





# [5.11.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.10.0...@koober/eslint-config@5.11.0) (2021-08-12)


### Bug Fixes

* disable jest/no-export rule ([b69cffc](https://gitlab.com/koober-sas/project-config/commit/b69cffc7fcf50dd465ad02620799afefc72bffa4))


### Features

* enable jest/no-commented-out-tests rule ([fa1c939](https://gitlab.com/koober-sas/project-config/commit/fa1c9398476e7226bb9ead7a3ada199cfdcbeeb2))





# [5.10.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.9.0...@koober/eslint-config@5.10.0) (2021-08-12)


### Bug Fixes

* make eslint-config looser in jest ([d782d68](https://gitlab.com/koober-sas/project-config/commit/d782d6827e19a7aecb639a53a05607fb0701bcb9))


### Features

* use jest recommended settings ([6ab2340](https://gitlab.com/koober-sas/project-config/commit/6ab234073f94bc311125a9e73b3c37317607db63))





# [5.9.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.8.1...@koober/eslint-config@5.9.0) (2021-08-11)


### Bug Fixes

* **deps:** update dependency eslint-plugin-jsdoc to v36 ([55a406c](https://gitlab.com/koober-sas/project-config/commit/55a406c958e95730e215fb0fbfd1a168104d8abe))
* **deps:** update dependency eslint-plugin-unicorn to v35 ([4b1d870](https://gitlab.com/koober-sas/project-config/commit/4b1d8702bcf4600b63a750d7b55f72549e018b4e))


### Features

* **eslint-config:** add recommended settings for typescript ([cb13c21](https://gitlab.com/koober-sas/project-config/commit/cb13c2130f24039079776c9e80477806ab5c8bcd))





## [5.8.1](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.8.0...@koober/eslint-config@5.8.1) (2021-07-27)

**Note:** Version bump only for package @koober/eslint-config





# [5.8.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.7.0...@koober/eslint-config@5.8.0) (2021-07-26)


### Features

* **eslint-config:** enable @typescript-eslint/no-base-to-string rule ([e469804](https://gitlab.com/koober-sas/project-config/commit/e4698045453025c587ddc95dc83a0cb297be78b3))
* **eslint-config:** enable @typescript-eslint/no-implicit-any-catch rule ([9c585ac](https://gitlab.com/koober-sas/project-config/commit/9c585aced1381e880ab642d713be12d59cbe50b0))
* **eslint-config:** enable @typescript-eslint/no-unnecessary-condition rule ([8b83755](https://gitlab.com/koober-sas/project-config/commit/8b837551702e82ce090dc0e8d0ad2d89575a2644))
* **eslint-config:** enable @typescript-eslint/no-unsafe-argument rule ([e004732](https://gitlab.com/koober-sas/project-config/commit/e004732ffde3375935bb7edc0be489f4a38f1c3b))
* **eslint-config:** enable @typescript-eslint/prefer-reduce-type-parameter rule ([903c598](https://gitlab.com/koober-sas/project-config/commit/903c59816474406c6c58df9b1fd945365f6ae4ff))
* **eslint-config:** enable @typescript-eslint/strict-boolean-expressions rule ([f2abe7c](https://gitlab.com/koober-sas/project-config/commit/f2abe7cdc75fc1fb4201c262867b26acf6ca7305))
* **eslint-config:** enable @typescript-eslint/switch-exhaustiveness-check rule ([934796c](https://gitlab.com/koober-sas/project-config/commit/934796c1c4bcc433628760427a39086be63cdcd2))
* **eslint-config:** enable getter-return rule ([5881066](https://gitlab.com/koober-sas/project-config/commit/5881066a850456f20ed2332f5416132287ef03ba))
* **eslint-config:** enable no-await-in-loop check ([86d4a03](https://gitlab.com/koober-sas/project-config/commit/86d4a0343614f020a900b74ac0c1d135a929a6cf))
* **eslint-config:** enable no-debugger rule ([79cf6ae](https://gitlab.com/koober-sas/project-config/commit/79cf6ae172aa332a7936a0ca4cb261f03ab64e26))
* **eslint-config:** enable no-dupe-else-if rule ([b1fc7a4](https://gitlab.com/koober-sas/project-config/commit/b1fc7a42ceb0322b166f09c00a59171e9589673a))
* **eslint-config:** enable no-extra-boolean-cast rule ([4527829](https://gitlab.com/koober-sas/project-config/commit/452782953b71884ee6457dcb232bd9e85a9863d5))
* **eslint-config:** enable no-import-assign rule ([cfdeaa8](https://gitlab.com/koober-sas/project-config/commit/cfdeaa86cd9f633a24ae3690be2a191f80d09505))
* **eslint-config:** enable no-loss-of-precision check ([5ca11da](https://gitlab.com/koober-sas/project-config/commit/5ca11da8664a48c8540787f94d4afb41d3a9d562))
* **eslint-config:** enable no-promise-executor-return rule ([17f99ce](https://gitlab.com/koober-sas/project-config/commit/17f99cea6e24bef69e1ed5b84dbce99f84b97fc1))
* **eslint-config:** enable no-prototype-builtins rule ([d5f173e](https://gitlab.com/koober-sas/project-config/commit/d5f173efc54550a29dccb3a8d3e1acaf603c9024))
* **eslint-config:** enable no-setter-return rule ([ef1753d](https://gitlab.com/koober-sas/project-config/commit/ef1753db8fa7cb1880018dd2eb0071d602973ae9))
* **eslint-config:** enable no-unreachable rule ([e2d48bc](https://gitlab.com/koober-sas/project-config/commit/e2d48bca69694527773a0dbece476aee1a5c1e1a))
* **eslint-config:** enable no-unreachable-loop rule ([01c2833](https://gitlab.com/koober-sas/project-config/commit/01c2833cb64aab38299c5381cb108b38588ce7f8))
* **eslint-config:** enable no-unsafe-optional-chaining rule ([cd0f0a7](https://gitlab.com/koober-sas/project-config/commit/cd0f0a7fd5e2223d6a486661fec7a0fc6fda9e44))
* **eslint-config:** enable no-useless-backreference rule ([41aa59c](https://gitlab.com/koober-sas/project-config/commit/41aa59c3539151779e5df59c3143784b08467ba9))
* **eslint-config:** enable valid-typeof rule ([11035ab](https://gitlab.com/koober-sas/project-config/commit/11035ab5b497e540c228db97de2c61d42a1298fb))
* **eslint-config:** make @typescript-eslint/consistent-type-assertions stricter ([3e85dbf](https://gitlab.com/koober-sas/project-config/commit/3e85dbf26b0cd041b5115d2582f3452008060ca6))





# [5.7.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.8...@koober/eslint-config@5.7.0) (2021-07-22)


### Features

* **eslint-config:** change level to error for "@typescript-eslint/no-non-null-assertion" rule ([78cf1a7](https://gitlab.com/koober-sas/project-config/commit/78cf1a7afee582e6e42b880c2c57ed508faca8cb))





## [5.6.8](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.7...@koober/eslint-config@5.6.8) (2021-07-22)


### Bug Fixes

* correct @typescript-eslint/dot-notation ([80dacb7](https://gitlab.com/koober-sas/project-config/commit/80dacb760cb07662d6e6c5c20924a7262bec1541))





## [5.6.7](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.6...@koober/eslint-config@5.6.7) (2021-07-22)


### Bug Fixes

* **eslint-config:** correct dot-notation compatibility ([c0f23be](https://gitlab.com/koober-sas/project-config/commit/c0f23be7eb69f4d86c7c547ccebd5037ba6a848b))
* **eslint-config:** correct dot-notation compatibility ([902fba6](https://gitlab.com/koober-sas/project-config/commit/902fba61180048a6aeb6742baee36430b58878b0))





## [5.6.6](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.5...@koober/eslint-config@5.6.6) (2021-07-20)

**Note:** Version bump only for package @koober/eslint-config





## [5.6.5](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.4...@koober/eslint-config@5.6.5) (2021-07-19)

**Note:** Version bump only for package @koober/eslint-config





## [5.6.4](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.3...@koober/eslint-config@5.6.4) (2021-07-13)

**Note:** Version bump only for package @koober/eslint-config





## [5.6.3](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.2...@koober/eslint-config@5.6.3) (2021-07-09)


### Bug Fixes

* **deps:** update dependency eslint-plugin-unicorn to v34 ([becb3bf](https://gitlab.com/koober-sas/project-config/commit/becb3bf59d45fbf1d378b9685c3c51f934885187))





## [5.6.2](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.1...@koober/eslint-config@5.6.2) (2021-07-07)


### Bug Fixes

* enable @typescript-eslint/no-require-imports reporting ([e09d7c9](https://gitlab.com/koober-sas/project-config/commit/e09d7c9487b0db54a14d189b95217957cca23152))





## [5.6.1](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.6.0...@koober/eslint-config@5.6.1) (2021-07-02)


### Bug Fixes

* **deps:** update dependency eslint-plugin-jsdoc to v35 ([8bc458a](https://gitlab.com/koober-sas/project-config/commit/8bc458aad445b52a61b6e944febe17902c92e7f7))
* **deps:** update dependency eslint-plugin-unicorn to v33 ([2c7b689](https://gitlab.com/koober-sas/project-config/commit/2c7b689443216c22fde46930945eb0931d932e8a))





# [5.6.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.11...@koober/eslint-config@5.6.0) (2021-06-02)


### Features

* improve leading and trailing underscore for member like ([7c67205](https://gitlab.com/koober-sas/project-config/commit/7c67205f26fd1a0a44644ec391e7c8efa2288947))





## [5.5.11](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.10...@koober/eslint-config@5.5.11) (2021-05-26)


### Bug Fixes

* **deps:** update dependency eslint-plugin-unicorn to v32 ([51a42bc](https://gitlab.com/koober-sas/project-config/commit/51a42bccabfaf41862aed78a39c1e98f7acf2f8a))





## [5.5.10](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.9...@koober/eslint-config@5.5.10) (2021-05-17)

**Note:** Version bump only for package @koober/eslint-config





## [5.5.9](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.8...@koober/eslint-config@5.5.9) (2021-05-14)

**Note:** Version bump only for package @koober/eslint-config





## [5.5.8](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.7...@koober/eslint-config@5.5.8) (2021-05-06)


### Bug Fixes

* **deps:** update dependency eslint-plugin-unicorn to v31 ([305506e](https://gitlab.com/koober-sas/project-config/commit/305506e8f34bfa8de960ce12ab7ff4c2a5b75043))





## [5.5.7](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.6...@koober/eslint-config@5.5.7) (2021-04-26)


### Bug Fixes

* **mrm:** add markdown magic config to eslint import extraneous config ([3523ede](https://gitlab.com/koober-sas/project-config/commit/3523edeee537b642a65a6782db832c1ba13d43c0))





## [5.5.6](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.5...@koober/eslint-config@5.5.6) (2021-04-26)


### Bug Fixes

* **eslint:** add typescript extension in extraneous deps ([4ddd8fb](https://gitlab.com/koober-sas/project-config/commit/4ddd8fb704b616d621bed83851aea3f1cbbd4d02))





## [5.5.5](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.4...@koober/eslint-config@5.5.5) (2021-04-23)


### Bug Fixes

* disable @typescript-eslint/no-redeclare ([bb688ad](https://gitlab.com/koober-sas/project-config/commit/bb688ad1de6ab9cd4718bd4038426e97db5bd8e3))





## [5.5.4](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.3...@koober/eslint-config@5.5.4) (2021-04-23)


### Bug Fixes

* add no redeclare for typescript ([022d740](https://gitlab.com/koober-sas/project-config/commit/022d740a024681970851696e7dafe2de53abaa8b))
* correct missing extensions ([d882701](https://gitlab.com/koober-sas/project-config/commit/d8827019aeec4f74954235295795a8aafcb5e8d1))





## [5.5.3](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.2...@koober/eslint-config@5.5.3) (2021-04-23)


### Bug Fixes

* adjust some import plugin rules ([f36365e](https://gitlab.com/koober-sas/project-config/commit/f36365e39ce41a993d163aa815fb79eda85c04cc))





## [5.5.2](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.1...@koober/eslint-config@5.5.2) (2021-04-22)

**Note:** Version bump only for package @koober/eslint-config





## [5.5.1](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.5.0...@koober/eslint-config@5.5.1) (2021-04-20)


### Bug Fixes

* **eslint:** disable promise/prefer-await-to-callbacks and promise/prefer-await-to-then ([70a70ce](https://gitlab.com/koober-sas/project-config/commit/70a70ce2d14f2d8f7c508b20988f709c6c511ce5))
* **mrm:** add exception for x, y, z in id-length ([f01d78d](https://gitlab.com/koober-sas/project-config/commit/f01d78dedaffaf211071d4b23aea4922de039882))





# [5.5.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.4.5...@koober/eslint-config@5.5.0) (2021-04-19)


### Bug Fixes

* **deps:** update dependency eslint-plugin-promise to v5 ([7eeff85](https://gitlab.com/koober-sas/project-config/commit/7eeff85a094d741d353bb37bc8c9289484a62af6))


### Features

* **eslint:** change error level to error for id-length ([d6a1d7a](https://gitlab.com/koober-sas/project-config/commit/d6a1d7a80d97d26e126791a34cdf04c92f223cec))





## [5.4.5](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.4.4...@koober/eslint-config@5.4.5) (2021-04-09)

**Note:** Version bump only for package @koober/eslint-config





## [5.4.4](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.4.3...@koober/eslint-config@5.4.4) (2021-03-30)


### Bug Fixes

* **deps:** update dependency eslint-plugin-unicorn to v29 ([9340b72](https://gitlab.com/koober-sas/project-config/commit/9340b7251aa7a842eee36e5bba53182ebd126888))





## [5.4.3](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.4.2...@koober/eslint-config@5.4.3) (2021-03-22)


### Bug Fixes

* **eslint:** add prettier patch after react config ([79bd767](https://gitlab.com/koober-sas/project-config/commit/79bd76755f475a4c2d34fa6493d3ef91ed21648b))
* **eslint:** correct flowtype configuration with prettier ([0349ff5](https://gitlab.com/koober-sas/project-config/commit/0349ff54357adbd95a50214e3041da1d6d4c6454))





## [5.4.2](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.4.1...@koober/eslint-config@5.4.2) (2021-03-16)


### Bug Fixes

* **eslint:** allowSingleExtends for real ([652d8d1](https://gitlab.com/koober-sas/project-config/commit/652d8d1a58f9b2bf4b03745b29b99ad4ef87aeed))





## [5.4.1](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.4.0...@koober/eslint-config@5.4.1) (2021-03-16)


### Bug Fixes

* **eslint:** allow no-empty-interface single extends ([6863fdc](https://gitlab.com/koober-sas/project-config/commit/6863fdcd5014cd2c9eec25070c296e16c7a27e61))





# [5.4.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.8...@koober/eslint-config@5.4.0) (2021-03-10)


### Bug Fixes

* **deps:** update dependency eslint-plugin-jsdoc to v32 ([2ef862a](https://gitlab.com/koober-sas/project-config/commit/2ef862af73c289173421f3b9188b95efa23ab1da))


### Features

* **eslint:** disable @typescript-eslint/explicit-module-boundary-types ([6fe5acb](https://gitlab.com/koober-sas/project-config/commit/6fe5acbf0ef9de5ad3c45a4cab2670e0e16b5b88))





## [5.3.8](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.7...@koober/eslint-config@5.3.8) (2021-03-01)


### Bug Fixes

* **deps:** update dependency eslint-plugin-unicorn to v28 ([4073b41](https://gitlab.com/koober-sas/project-config/commit/4073b410f54a279cfa2f96e1d27b808fa1e9b6f3))





## [5.3.7](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.6...@koober/eslint-config@5.3.7) (2021-02-24)

**Note:** Version bump only for package @koober/eslint-config





## [5.3.6](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.5...@koober/eslint-config@5.3.6) (2021-02-23)

**Note:** Version bump only for package @koober/eslint-config





## [5.3.5](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.4...@koober/eslint-config@5.3.5) (2021-02-22)


### Bug Fixes

* **eslint:** cleanup some babel and eslint deps ([3f32a22](https://gitlab.com/koober-sas/project-config/commit/3f32a22b3c5f05371060f4f45e97f87d1d76a9cb))





## [5.3.4](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.3...@koober/eslint-config@5.3.4) (2021-02-22)

**Note:** Version bump only for package @koober/eslint-config





## [5.3.3](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.2...@koober/eslint-config@5.3.3) (2021-02-22)


### Bug Fixes

* **deps:** add peer deps as loose dependency ([666235c](https://gitlab.com/koober-sas/project-config/commit/666235c56c922368ef6c0e8274c544a2055bffc8))


### Reverts

* delete @koober/eslint-config@5.3.2 ([47d3e03](https://gitlab.com/koober-sas/project-config/commit/47d3e0393dfeee12fda7254becf7f56cd1602db7))





## [5.3.2](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.1...@koober/eslint-config@5.3.2) (2021-02-22)


### Reverts

* delete @koober/eslint-config@5.3.2 ([47d3e03](https://gitlab.com/koober-sas/project-config/commit/47d3e0393dfeee12fda7254becf7f56cd1602db7))





## [5.3.2](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.1...@koober/eslint-config@5.3.2) (2021-02-19)

**Note:** Version bump only for package @koober/eslint-config





## [5.3.1](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.3.0...@koober/eslint-config@5.3.1) (2021-02-15)

**Note:** Version bump only for package @koober/eslint-config





# [5.3.0](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.2.7...@koober/eslint-config@5.3.0) (2021-02-12)


### Features

* **mrm:** migrate from babel-eslint to @babel/eslint-parser ([35a85c0](https://gitlab.com/koober-sas/project-config/commit/35a85c0056a972d4bb2faaaf030a9143acb89693))





## [5.2.7](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.2.6...@koober/eslint-config@5.2.7) (2021-02-01)


### Bug Fixes

* **deps:** update dependency eslint-plugin-unicorn to v27 ([523230a](https://gitlab.com/koober-sas/project-config/commit/523230a13381b05853cc10c9f8e0a108b36a65bf))





## [5.2.6](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.2.5...@koober/eslint-config@5.2.6) (2021-01-27)

**Note:** Version bump only for package @koober/eslint-config





## [5.2.5](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.2.4...@koober/eslint-config@5.2.5) (2021-01-19)


### Bug Fixes

* **eslint-config:** disable conflictuous  prettier rules ([bee0bc0](https://gitlab.com/koober-sas/project-config/commit/bee0bc0bc40fe73d492b35f68939b97414eef82a))





## [5.2.4](https://gitlab.com/koober-sas/project-config/compare/@koober/eslint-config@5.2.3...@koober/eslint-config@5.2.4) (2021-01-18)


### Bug Fixes

* **test:** correct tests ([dd3db66](https://gitlab.com/koober-sas/project-config/commit/dd3db669aa6713a3bc45112aa7a78773e740a2d1))
